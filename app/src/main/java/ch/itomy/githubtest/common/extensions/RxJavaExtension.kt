package ch.itomy.githubtest.common.extensions

import io.reactivex.Flowable
import org.reactivestreams.Publisher

fun <T> Publisher<T>.toFlowable() = Flowable.fromPublisher(this)