package ch.itomy.githubtest.data.repository.auth

import ch.itomy.githubtest.common.Result
import io.reactivex.Flowable

interface AuthRepository {
    fun isUserAuthorized(): Flowable<Boolean>

    fun auth(code: String): Flowable<Result<Unit>>
}