package ch.itomy.githubtest.data.source.auth.remote

import ch.itomy.githubtest.common.Result
import io.reactivex.Flowable

interface AuthRemoteDataSource {
    fun auth(code: String): Flowable<Result<String>>
}