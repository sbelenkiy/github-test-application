package ch.itomy.githubtest.data.source.auth.remote

import ch.itomy.githubtest.common.Result
import ch.itomy.githubtest.data.network.service.AuthService
import io.reactivex.Flowable
import java.io.IOException
import javax.inject.Inject

class DefaultAuthRemoteDataSource @Inject constructor(
    private val authService: AuthService
) : AuthRemoteDataSource {

    override fun auth(code: String): Flowable<Result<String>> = authService.auth(code)
        .map {
            val token = it.response()?.body()?.accessToken
            val isSuccessful = it.response()?.isSuccessful == true
            return@map if (isSuccessful && token != null) {
                Result.Success(token)
            } else if (it.isError) {
                Result.Error(it.error()!!)
            } else {
                Result.Error(IOException(""))
            }
        }
}