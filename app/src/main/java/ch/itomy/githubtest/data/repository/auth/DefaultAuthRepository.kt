package ch.itomy.githubtest.data.repository.auth

import ch.itomy.githubtest.common.Result
import ch.itomy.githubtest.data.source.auth.local.AuthLocalDataSource
import ch.itomy.githubtest.data.source.auth.remote.AuthRemoteDataSource
import io.reactivex.Flowable
import io.reactivex.Observable
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

class DefaultAuthRepository @Inject constructor(
    private val authLocalDataSource: AuthLocalDataSource,
    private val authRemoteDataSource: AuthRemoteDataSource
) : AuthRepository {

    override fun isUserAuthorized() =
        Flowable.fromCallable { !authLocalDataSource.token.isNullOrEmpty() }

    override fun auth(code: String): Flowable<Result<Unit>> =
        authRemoteDataSource.auth(code)
            .doOnEach {
                it.value?.let { result ->
                    if (result is Result.Success) {
                        authLocalDataSource.token = result.data
                    }
                }

            }
            .onErrorReturn { Result.Error(it) }
            .map { it.map { Unit } }
}