package ch.itomy.githubtest.domain

import ch.itomy.githubtest.data.repository.auth.AuthRepository
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CheckIsUserAuthorizedUseCase @Inject constructor(
    private val repository: AuthRepository
) {

    fun invoke(): Flowable<Boolean> =
        repository.isUserAuthorized()
            .observeOn(Schedulers.io())
}