package ch.itomy.githubtest.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import ch.itomy.githubtest.common.Result
import ch.itomy.githubtest.data.repository.auth.AuthRepository
import javax.inject.Inject

class AuthUseCase @Inject constructor(
    private val repository: AuthRepository
) {
    fun invoke(code: String): LiveData<Result<Unit>> =
        LiveDataReactiveStreams.fromPublisher(repository.auth(code))
}