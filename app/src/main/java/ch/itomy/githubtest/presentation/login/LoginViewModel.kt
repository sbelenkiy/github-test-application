package ch.itomy.githubtest.presentation.login

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import ch.itomy.githubtest.domain.AuthUseCase
import javax.inject.Inject

class LoginViewModel @Inject constructor(
    private val authUseCase: AuthUseCase
) : ViewModel() {
    private val authCode = MutableLiveData<String>()

    val authResult = authCode.switchMap {
        authUseCase.invoke(it)
    }

    fun auth(code: String) = authCode.postValue(code)
}