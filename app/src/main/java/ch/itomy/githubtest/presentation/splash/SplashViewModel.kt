package ch.itomy.githubtest.presentation.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.toLiveData
import ch.itomy.githubtest.domain.CheckIsUserAuthorizedUseCase
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    checkIsUserAuthorizedUseCase: CheckIsUserAuthorizedUseCase
) : ViewModel() {
    val isUserAuthorized = checkIsUserAuthorizedUseCase.invoke().toLiveData()
}